# -*- encoding: utf-8 -*-
require File.expand_path('../lib/unrar/version', __FILE__)

Gem::Specification.new do |gem|
  gem.authors       = ["Pinho"]
  gem.email         = ["ti@pinho.com.br"]
  gem.description   = ["ti@pinho.com.br"]
  gem.summary       = ["ti@pinho.com.br"]
  gem.homepage      = ""

  gem.files         = `git ls-files`.split($\)
  gem.executables   = gem.files.grep(%r{^bin/}).map{ |f| File.basename(f) }
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  gem.name          = "unrar"
  gem.require_paths = ["lib"]
  gem.version       = Unrar::VERSION

  gem.add_development_dependency 'rspec', '~> 3.4'
  gem.add_development_dependency 'autotest'
end
