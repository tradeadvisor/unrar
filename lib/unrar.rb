require "unrar/version"
require 'fileutils'
require 'tmpdir'

module Unrar

  class Archive

    
    attr_accessor :file, :tmpdir

    def initialize(file)
      @file = file
      @tmpdir = Dir.mktmpdir
      at_exit { FileUtils.rm_rf @tmpdir }

      unless File.readable? file
        raise "Cannot read #{file}!"
      end
    end

    def extract(filename, *filenames)
      `#{Archive.unrar} -y x #{@file} #{filename} #{filenames.join(" ")} #{tmpdir}/`
      Dir["#{tmpdir}/**/*"].to_ary
    end

    def list
      cmdoutput = `#{Archive.unrar} lb #{@file}`
      cmdoutput.split("\n").map { |line| line.chomp }
    end

    def each(&block)
      list.each do |file|
        block.call(file)
      end
    end

    def self.unrar
      @@unrar ||= search_for
    end

    def self.open(filename, &block)
      archive = Archive.new(filename)
      block.call(archive)
    end

    private

    def self.search_for
      $:.find { |i| File.file?(File.join(i, "rar")) } + "/rar"
    end

  end
end
