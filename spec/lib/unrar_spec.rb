require 'unrar'

describe Unrar::Archive do
  describe "finding the unrar executable" do

    before do
      Unrar::Archive.class_variable_set(:@@unrar, nil)
    end

    context "on search path" do

      before do
        Unrar::Archive.should_receive(:search_for).and_return("./lib/rar")
      end

      it "should find unrar" do
        Unrar::Archive.unrar.should == "./lib/rar"
      end

    end

  end

  describe "should be able to extract" do
    
    subject { Unrar::Archive.new('spec/fixtures/test.rar') }

    it "a single file" do
      subject.extract('001.jpg').should_not be_nil
    end

    it "multiple files" do
      expect(subject.extract('001.jpg', '002.jpg').size).to eq(2)
    end

  end

  describe "should be able to by block" do

    it "a single file" do
      Unrar::Archive.open('spec/fixtures/test.rar') do |item|
        expect(item.extract('001.jpg').count).to eq(1)
      end
    end


    it "multiple files" do
      Unrar::Archive.open('spec/fixtures/test.rar') do |item|
        expect(item.extract('001.jpg', '002.jpg').size).to eq(2)
      end
    end

  end

  describe "should be able to list files" do
    subject { Unrar::Archive.new('spec/fixtures/test.rar') }

    it "Correctly list all files" do
      expect(subject.list.size).to eq(3)
    end
  end


    describe "should be able to by block" do

    it "a single file" do
      Unrar::Archive.open('spec/fixtures/test.rar') do |item|
        expect(item.extract('001.jpg').count).to eq(1)
      end
    end


    it "multiple files" do
      Unrar::Archive.open('spec/fixtures/test.rar') do |item|
        expect(item.extract('001.jpg', '002.jpg').size).to eq(2)
      end
    end

  end

  describe "each" do
    subject { Unrar::Archive.new('spec/fixtures/test.rar') }

    it "should iterate over all files" do
      subject.each do |entry|
        expect(entry).to  be_a_kind_of(String)
      end
    end
  end

end
